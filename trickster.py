import os
import argparse
import logging
import socket

from conf import Configuration
from http import run_http_app
from manager.storage import ShelfStorage
from manager.pool import Pool
from proc.tunnel import TunnelProcess
from proc.message import Reader, Writer

parser = argparse.ArgumentParser(description='Trickster creates continuously '
                                             'ssh tunnels for domains.')
parser.add_argument('-c', '--config', help='Configuration file for trickster',
                    default='trickster.conf')
args = parser.parse_args()

logging.basicConfig()


def tunnel_pool(conf, reader):
    database = ShelfStorage(conf.storage.filepath)
    pool = Pool(conf.provider, database)
    pool.connect()

    proc = TunnelProcess()
    logging.info('[Trickster]: getting an instance to make tunnel.')
    for instance in pool:
        cmd = conf.tunneller.command(instance)
        proc.run(cmd, reader.wait)

        logging.info('[Trickster]: got message. Changing tunnel.')
        proc.kill()

    proc.kill()


def forked_tunnel_pool(conf, reader):
    pid = os.fork()
    if pid == 0:
        tunnel_pool(conf, reader)
    else:
        os.wait()


def main():
    conf = Configuration.from_file(args.config)

    writer, reader = socket.socketpair()
    pid = os.fork()
    if pid == 0:
        logging.info('[Trickster]: starting http interface application.')
        reader.close()
        run_http_app(conf.http_interface, Writer(writer))
    else:
        writer.close()
        forked_tunnel_pool(conf, Reader(reader,
                                        int(conf.provider.instance_min_ttl)))
        os.wait()


if __name__ == '__main__':
    main()
