import json
from functools import wraps

from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop

from conf.sections import HttpInterface


def basic_auth(auth_func):
    def _request_auth(handler):
        handler.set_header('WWW-Authenticate', 'Basic realm=PTR')
        handler.set_status(401)
        handler.finish()
        return

    def decorate(method):
        @wraps(method)
        def inner(handler, *args, **kwargs):
            auth_header = handler.request.headers.get('Authorization')
            if not auth_header:
                return _request_auth(handler)

            if not auth_header.startswith('Basic '):
                return _request_auth(handler)

            if not auth_func(auth_header[6:]):
                return _request_auth(handler)

            return method(handler, *args, **kwargs)
        return inner

    return decorate


def make_handler(http_interface):
    def _get(handler, *args, **kwargs):
        writer = handler.application.settings.get('writer')
        touched_at, acted_at = writer.touch()

        handler.set_header('Content-Type', 'application/json')
        raw_out = {'succeeded': True, 'touched_at': touched_at,
                   'acted_at': acted_at, 'binded': touched_at == acted_at}
        handler.write(json.dumps(raw_out))
        handler.set_status(200)

    handler = type('TriggerHandler', (RequestHandler, ),
                   {'get': basic_auth(http_interface.authenticate)(_get)})
    return handler


def run_http_app(http_interface, writer):
    assert isinstance(http_interface, HttpInterface), "http_interface " \
                                                      "parameter should be " \
                                                      "an instance of " \
                                                      "HttpInterface"
    handler = make_handler(http_interface)
    app = Application(handlers=[(r'/', handler)],
                      debug=True, autoreload=True, writer=writer)
    app.listen(http_interface.listen)
    IOLoop.current().start()
