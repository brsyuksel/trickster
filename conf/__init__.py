import os
from ConfigParser import ConfigParser, NoSectionError, NoOptionError
from functools import partial
import logging

from exceptions import ConfigFileMissed
from sections import Provider, Tunneller, HttpInterface, Storage


logging.basicConfig(level=logging.DEBUG)


def read_configuration(filepath):
    if not os.path.exists(filepath):
        raise ConfigFileMissed(filepath)

    conf = ConfigParser()
    conf.read(filepath)

    return conf


class Configuration(object):
    def __init__(self, provider, tunneller, http_interface, storage):
        assert isinstance(provider, Provider), "provider parameter should be " \
                                               "an instance of Provider"
        assert isinstance(tunneller, Tunneller), "tunnel parameter should be " \
                                                 "an instance of Tunneller"
        assert isinstance(http_interface, HttpInterface), "http_interface " \
                                                          "parameter should " \
                                                          "be an instance of " \
                                                          "HttpInterface"
        assert isinstance(storage, Storage), "storage parameter should be " \
                                             "an instance of Storage"

        self.provider = provider
        self.tunneller = tunneller
        self.http_interface = http_interface
        self.storage = storage

    @classmethod
    def from_file(cls, filepath):
        try:
            conf = read_configuration(filepath)
        except ConfigFileMissed:
            logging.exception('[Configuration]: file not found')
            return None

        try:
            section = partial(conf.get, 'provider')
            provider = Provider(
                name=section('name'), token=section('token'),
                region_list=section('region_list'),
                instance_name_prefix=section('instance_name_prefix'),
                ssh_key_pair_name=section('ssh_key_pair_name'),
                instances=section('instances'), size_id=section('size_id'),
                image_id=section('image_id'),
                instance_min_ttl=section('instance_min_ttl'))

            section = partial(conf.get, 'tunneller')
            tunneller = Tunneller(listen=section('listen'),
                                  forward=section('forward'),
                                  domain=section('domain'))

            section = partial(conf.get, 'http_interface')
            http_interface = HttpInterface(listen=section('listen'),
                                           username=section('username'),
                                           password=section('password'))

            section = partial(conf.get, 'storage')
            storage = Storage(filepath=section('filepath'))

        except (NoOptionError, NoSectionError):
            logging.exception('[Configration]: malformed conf file')
            return None

        instance = cls(provider, tunneller, http_interface, storage)
        return instance
