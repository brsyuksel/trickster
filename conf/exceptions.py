
class ConfigFileMissed(Exception):
    """
    Raises when provided config file is not found.
    """
