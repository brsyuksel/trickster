import logging
from collections import namedtuple
from base64 import b64decode

from manager.datatype import Instance

_Provider = namedtuple('_Provider', ('name', 'token', 'region_list',
                                     'instance_name_prefix',
                                     'ssh_key_pair_name', 'instances',
                                     'size_id', 'image_id',
                                     'instance_min_ttl', ))

_Tunneller = namedtuple('_Tunneller', ('listen', 'forward', 'domain', ))

_HttpInterface = namedtuple('_HttpInterface', ('listen', 'username',
                                               'password', ))

_Storage = namedtuple('_Storage', ('filepath', ))

logging.basicConfig(level=logging.DEBUG)


class Provider(_Provider):
    """
    extended provided
    """
    @property
    def regions(self):
        reg = self.region_list[1:-1]
        return map(lambda i: i.strip(), reg.split(','))

    @property
    def driver_args(self):
        def _do():
            return (self.token,), {'api_version': 'v2'}

        def _vultr():
            return (self.token,), {}

        fns = {'digitalocean': _do, 'vultr': _vultr}
        return fns.get(self.name, lambda: ((self.token,), {}))()

    @property
    def login_name(self):
        return 'root'


class Tunneller(_Tunneller):
    """
    extended tunnel
    """
    def command(self, instance):
        assert isinstance(instance, Instance), "instance parameter should be " \
                                               "an instance of Instance"

        cmd = 'ssh -o StrictHostKeyChecking=no -o ServerAliveInterval=30 ' \
              '-nNT -L {listen}:{domain}:{forward} {username}@{ip_address}'
        return cmd.format(listen=self.listen, domain=self.domain,
                          forward=self.forward, username=instance.username,
                          ip_address=instance.ip_address)


class HttpInterface(_HttpInterface):
    """
    extended http interface
    """

    def authenticate(self, basic_auth):
        try:
            decoded_credentials = b64decode(basic_auth)
            username, password = decoded_credentials.split(':')
            return username == self.username and password == self.password
        except (TypeError, ValueError):
            return False
        except:
            logging.exception('[HttpInterface]: Raised an exception')

        return False


class Storage(_Storage):
    """
    extended storage
    """
