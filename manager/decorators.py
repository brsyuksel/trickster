from functools import wraps


def should_not_none(attr_name, raised, msg):
    def decorate(method):
        @wraps(method)
        def fn(slf, *args, **kwargs):
            if not getattr(slf, attr_name):
                raise raised(msg)
            return method(slf, *args, **kwargs)
        return fn
    return decorate
