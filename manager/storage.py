import shelve


class ShelfStorage(object):
    def __init__(self, filepath):
        self._s = shelve.open(filepath, writeback=True)

    @property
    def items(self):
        if 'items' not in self._s:
            self._s['items'] = []
            self._s.sync()
        return self._s['items']

    def add_item(self, item):
        self._s['items'].append(item)
        self._s.sync()

    def remove_item(self, item):
        self._s['items'].remove(item)
        self._s.sync()

    def close(self):
        self._s.close()
