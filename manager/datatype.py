from collections import namedtuple

_Instance = namedtuple('_Instance', ('name', 'username', 'ip_address'))


class Instance(_Instance):
    """
    extended trickster
    """
