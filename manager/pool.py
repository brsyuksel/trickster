import logging
from datetime import datetime
import time

from libcloud.compute.types import Provider, NodeState
from libcloud.compute.providers import get_driver
from libcloud.common.types import LibcloudError

from decorators import should_not_none
from datatype import Instance
from conf.sections import Provider as ProviderType

logging.basicConfig(level=logging.DEBUG)


class Pool(object):
    def __init__(self, provider_conf, storage):
        assert isinstance(provider_conf, ProviderType), "provider_conf " \
                                                        "parameter should be " \
                                                        "an instance of " \
                                                        "Provider"
        self._provider = provider_conf
        self._storage = storage
        self._conn = None
        self._nodes = []

        self.__size = None
        self.__image = None
        self.__key_pair = None
        self.__locations = []
        self._location_idx = 0

    @property
    def driver_class(self):
        providers = {
            'digitalocean': Provider.DIGITAL_OCEAN,
            'vultr': Provider.VULTR,
        }
        p = providers.get(self._provider.name)
        if not p:
            logging.error('[Pool]: provider is not found {}'.format(
                self._provider.name))
            raise KeyError('{} is not found'.format(self._provider.name))

        return get_driver(p)

    def connect(self):
        if self._conn:
            return

        args, kwargs = self._provider.driver_args
        driver = self.driver_class
        self._conn = driver(*args, **kwargs)

    def _to_instance(self, node):
        public_ip = str(node.public_ips[0])
        return Instance(name=node.name, ip_address=public_ip,
                        username=self._provider.login_name)

    @property
    def locations(self):
        if not self.__locations:
            self.__locations = filter(lambda l: l.id in self._provider.regions,
                                      self._conn.list_locations())
        return self.__locations

    @property
    def size(self):
        if not self.__size:
            self.__size = filter(
                lambda s: s.id == self._provider.size_id,
                self._conn.list_sizes())[0]
        return self.__size

    @property
    def image(self):
        if not self.__image:
            self.__image = filter(
                lambda i: i.id == self._provider.image_id,
                self._conn.list_images())[0]
        return self.__image

    @property
    def key_pair(self):
        if not self.__key_pair:
            self.__key_pair = filter(
                lambda k: k.name == self._provider.ssh_key_pair_name,
                self._conn.list_key_pairs())[0]
        return self.__key_pair

    def _pick_a_location(self):
        logging.info('[Pool]: current location index: {}'.format(
            self._location_idx))

        locations = self.locations
        loc = locations[self._location_idx % len(locations)]
        self._location_idx += 1
        return loc

    def _generate_a_name(self):
        utc = datetime.utcnow().strftime('%y%m%d%H%M%S')
        name = '{}{}'.format(self._provider.instance_name_prefix, utc)
        return name

    @should_not_none('_conn', RuntimeError, 'Pool is not connected.')
    def create_node(self):
        logging.info('[Pool]: creating a node.')

        name = self._generate_a_name()
        loc = self._pick_a_location()
        key_ids = [self.key_pair.id]

        node = self._conn.create_node(name, self.size, self.image, loc,
                                      ex_ssh_key_ids=key_ids)
        log_msg = "[Pool]: {name} named node is created with {image} " \
                  "image, {size} size, {key} key in {loc}"
        logging.info(log_msg.format(name=name, image=self.image.name,
                                    size=self.size.name,
                                    key=self.key_pair.name, loc=loc.name))
        return node

    @should_not_none('_conn', RuntimeError, 'Pool is not connected.')
    def create_nodes(self, count):
        return [self.create_node() for _ in range(count)]

    @should_not_none('_conn', RuntimeError, 'Pool is not connected.')
    def get_nodes(self):
        nodes = self._conn.list_nodes()
        all_ins = filter(
            lambda f: f.name.startswith(self._provider.instance_name_prefix),
            nodes)

        marked = filter(lambda n: n.name in self._storage.items, all_ins)
        instances = filter(
            lambda n: n.name not in self._storage.items, all_ins)

        if len(marked):
            logging.info(
                "[Pool]: {} nodes will be destroyed.".format(len(marked)))
            self.destroy_nodes(marked)
        return sorted(instances, key=lambda n: n.name)

    @should_not_none('_conn', RuntimeError, 'Pool is not connected.')
    def destroy_node(self, node):
        try:
            result = self._conn.destroy_node(node)
            if result and node.name in self._storage.items:
                self._storage.remove_item(node.name)
                logging.info("[Pool]: {} node is destroyed".format(node.name))
        except LibcloudError:
            logging.warn(
                "[Pool]: {} node couldn't destroyed.".format(node.name))
            self.mark_to_destroy(node)

        return False

    @should_not_none('_conn', RuntimeError, 'Pool is not connected.')
    def destroy_nodes(self, nodes):
        return map(self.destroy_node, nodes)

    def mark_to_destroy(self, node):
        if node.name not in self._storage.items:
            logging.info(
                "[Pool]: {} node is marked to destroy".format(node.name))
            self._storage.add_item(node.name)

    @should_not_none('_conn', RuntimeError, 'Pool is not connected.')
    def populate(self):
        nodes = self.get_nodes()
        count_diff = int(self._provider.instances) - len(nodes)
        if count_diff:
            logging.info(
                '[Pool]: {} instance(s) will be created.'.format(count_diff))
            new_nodes = self.create_nodes(count_diff)
            nodes += new_nodes

        self._nodes = sorted(nodes, key=lambda n: n.name)

    def wait_until_running(self, node):
        if node.state == NodeState.RUNNING \
                and str(node.public_ips[0]) != '0.0.0.0':
            return node

        logging.info("[Pool]: Waiting for {} node is being "
                     "running.".format(node.name))

        while True:
            nodes = self.get_nodes()
            wait_for = filter(lambda n: n.name == node.name, nodes)[0]
            if wait_for.state == NodeState.RUNNING \
                    and str(wait_for.public_ips[0]) != '0.0.0.0':
                # wait for the instance is able to establish a ssh connection
                time.sleep(10)
                return wait_for
            time.sleep(3)

    def __iter__(self):
        while True:
            self.populate()
            node = self._nodes[0]
            running_node = self.wait_until_running(node)
            yield self._to_instance(running_node)
            self.destroy_node(running_node)
