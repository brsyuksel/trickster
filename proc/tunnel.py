import os
import logging
import signal

logging.basicConfig()


class TunnelProcess(object):

    def __init__(self):
        self.__pid = None

    @property
    def pid(self):
        return self.__pid

    @staticmethod
    def extract_cmd_parts(cmd):
        parts = cmd.split()
        prog, args = parts[0], parts
        return prog, args

    def run(self, cmd, wait_fn=os.wait):
        if self.pid:
            logging.warning("[TunnelProcess]: There is already running "
                            "a command with {} pid".format(self.__pid))
            return

        prog, args = self.extract_cmd_parts(cmd)

        pid = os.fork()
        if pid == 0:
            logging.info('[TunnelProcess]: running command.')
            os.execvp(prog, args)
        else:
            self.__pid = pid
            wait_fn()

    def _kill(self, pid):
        try:
            return os.kill(pid, signal.SIGKILL)
        except OSError:
            return

    def kill(self):
        if not self.pid:
            return

        self._kill(self.pid)
        self.__pid = None
