import logging

from helpers import utc_timestamp

logging.basicConfig()


class Writer(object):
    def __init__(self, pair):
        self._pair = pair

    def touch(self):
        touched_at = utc_timestamp()
        logging.info(
            '[Writer]: Sending touch message at: {}'.format(touched_at))

        self._pair.sendall(str(touched_at))
        acted_at = self._pair.recv(10)
        logging.info('[Writer]: Received last action at: {}'.format(acted_at))
        return touched_at, int(acted_at)


class Reader(object):
    def __init__(self, pair, expires_in):
        self._pair = pair
        self._expires_in = expires_in
        self._acted_at = utc_timestamp()

    def _is_expired(self, timestamp):
        return self._acted_at + self._expires_in < timestamp

    def wait(self):
        while True:
            received = self._pair.recv(10)
            timestamp = int(received)
            if self._is_expired(timestamp):
                logging.info('[Reader]: Last action is expired.')
                self._acted_at = timestamp
                self._pair.sendall(str(timestamp))
                break
            else:
                self._pair.sendall(str(self._acted_at))
