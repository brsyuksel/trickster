from datetime import datetime
import time


def utc_timestamp():
    utc = datetime.utcnow()
    return int(time.mktime(utc.timetuple()))
